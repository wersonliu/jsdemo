//原型链继承
function father() {
    this.fa = true
}

father.prototype.getFa = function () {
    return this.fa
}

function son() {
    this.son = false
}

son.prototype = new father();

son.prototype.getSon = function () {
    return this.son
}

var s1 = new son();
//此时子对象已经拥有父对象的方法
console.log(s1.getFa());  //true
//子对象实例 是以下任意一个的实例
console.log(s1 instanceof Object);//true
console.log(s1 instanceof son);//true
console.log(s1 instanceof father);//true


//
function Father1() {
    this.fa = true
}

Father1.prototype.getFa1 = function () {
    return this.fa
};

function Son1() {
    this.son = false
}

Son1.prototype = new Father1();
Son1.prototype.getSon1 = function () {
    return this.son
};
//重写父类的方法会覆盖父类的同名方法
// Son1.prototype.getFa1() = function () {
//     return 'xxx'
// };

var s2 = new Son1();
console.log(s2.getSon1());


var x = 4,
    obj = {
        x: 3,
        bar: function() {
            var x = 2;
            setTimeout(function() {
                var x = 1;
                console.log(this.x);
            }, 1000);
        }
    };
// obj.bar();

var arr = [];
arr[0]  = 'a';
arr[1]  = 'b';
arr.foo = 'c';
console.log(arr.length);

//全局变量局部变量
(function() {
    // 严格模式需要你显式地引用全局作用域
    var a = b = 5;

})();
console.log(b);  //5  a 是局部，b是全局
// console.log(a);


String.prototype.myfun=String.prototype.myfun||function (times) {
    let str='';
    for (let i = 0; i <times ; i++) {
        str+=this
    }
    return str
};
// console.log('hello',myfun(3));


var fullname = 'John Doe';
var obj2 = {
    fullname: 'Colin Ihrig',
    prop: {
        fullname: 'Aurelio De Rosa',
        getFullname: function() {
            return this.fullname;
        }}};
console.log(obj2.prop.getFullname());
var test = obj2.prop.getFullname;
console.log(test());
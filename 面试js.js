if (typeof Array.isArray === "undefined") {
    Array.isArray = function (arg) {
        return Object.prototype.toString.call(arg) === "[object Array]"
    };
}


let arr = [1, 3, 5, 2, 8, 10, 32, 22, 17];

arr.sort(function (a, b) {
    return b - a
});
console.log(arr);
let myurl = "http://item.taobao.com/item.htm?a=1&b=2&c=&d=xxx&e";

function getKv(url) {
    let res = {};
    let s = myurl.split('?')[1];
    let kv = s.split('&');
    for (let i = 0; i < kv.length; i++) {
        res[kv[i].split('=')[0]] = kv[i].split('=')[1]
    }
    return res
}

console.log(getKv(myurl));


//caller
let a = function () {
    console.log(a.caller);
};
let b = function () {
    a();
};
b();
a();
//[Function: b]
// [Function]    顶层调用时返回null

//callee

let c = function () {
    console.log(arguments.callee);
};
let d = function () {
    c();
};
d();  // c  正在执行的函数本身的引用
c();

let arrr = [1, 2, 3, 3, 4, 4, 5, 5, 6, 1, 9, 3, 25, 4];

let ress = [];
for (const arrKey of arrr) {
    if (!(arrKey in ress)) {
        ress.push(arrKey)
    }
}
console.log(ress);
console.log(2 in arrr);

function print() {
    const args = Array.prototype.slice.call(arguments);  //为了使用unshift数组方法，将argument转化为真正的数组
    // args.unshift('(app)');

    console.log.apply(console, args);
}
print('你好');

function arrRandom(arr){
    arr.sort(function(){
        return 0.5-Math.random()
    })
}

const marr=[1,4,6,8,3,11,9]
arrRandom(marr)
console.log(marr)

const ms='as'

console.log(ms)
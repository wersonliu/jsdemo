function Fa(name) {
    this.color = ['red', 'green', 'blue'];
    this.getA = function () {
        return 'xxｘx'
    };
    this.name=name
}

function Son() {
    //通过apply 和 call 可以达到继承效果,apply 无法传递对象小参数
    // Fa.apply(this,'刘海生')
    Fa.call(this,'刘海生')
}

Son.prototype.getB = function () {
    return 'bbbb'
};
Son.prototype.getA = function () {
    return 'bbbb'
};

var s1 = new Son();
//创建一个实例,会拥有自己的属性副本
s1.color.push('black');
console.log(s1.color); //[ 'red', 'green', 'blue', 'black' ]
console.log(s1.getA());//xx  重写getA 还是调用父类的
console.log(s1.getB());//bbb
console.log(s1.name); //刘海生

var f1 = new Fa();
console.log(f1.color); //[ 'red', 'green', 'blue' ]

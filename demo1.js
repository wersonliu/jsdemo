// console.log('aa')
var util=require('util')

function Base(){
    this.name='base';
    this.base=1991;
    this.sayH=function(){
        console.log('hello '+this.name)
    };
}
Base.prototype.showName=function(){
    console.log(this.name);
};

function Sub() {
    this.name = 'sub';
}
util.inherits(Sub, Base);
var objBase = new Base();
objBase.showName();
objBase.sayH();
console.log(objBase);
var objSub = new Sub();
objSub.showName();
objSub.sayHe();
console.log(objSub);

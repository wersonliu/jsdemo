function Fat(name) {
    this.name = name;
    this.colors = ['red', 'blue', 'black']
}

Fat.prototype.sayN = function () {
    return this.name
}

function So(name, age) {
    Fat.call(this, name);
    this.age = age
}

So.prototype = new Fat();
So.prototype.constructor = So;
So.prototype.sayA = function () {
    return this.age
};

var so1 = new So('儿子对象', 29);
so1.colors.push('哈哈色');
console.log(so1.colors);  //[ 'red', 'blue', 'black', '哈哈色' ]
console.log(so1.sayA());  //29
console.log(so1.sayN()); //儿子对象

//组合继承是常见的方式


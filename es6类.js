
//传统es5写法
function Point(x,y){
    this.x=x;
    this.y=y;
}
Point.prototype.toString=function(){
    return '(' + this.x + ', ' + this.y + ')';
}
var p=new Point(1,2)

console.log(p);
//es6 类
class Mpoint{
    constructor(x,y){
        this.x=x;
        this.y=y;
    }
    mtoString(){
        return '(' + this.x + ', ' + this.y + ')';
    }
}
let mp=new Mpoint(2,3)
let a=mp.mtoString()
console.log(a);
console.log(typeof Mpoint); //function
console.log(Mpoint===Mpoint.prototype.constructor); //true

// 类的方法定义在prototype
//在类的实例上调用方法是 在调用原型的

//向类添加方法
Object.assign(Mpoint.prototype,{
    toValue(){}
})

let objkey=Object.getOwnPropertyNames(Mpoint.prototype)
let objk=Object.keys(Mpoint.prototype)
let objk5=Object.keys(Point.prototype)
//内部定义方法不可枚举
console.log(objk)//[ 'toValue' ]

//但是在es5中可以
console.log(objk5) //[ 'toString' ]

console.log(objkey) //[ 'constructor', 'mtoString', 'toValue' ]

class Fo{
    //构造方法可以为空
    constructor(){}
}

class Foo{
    //可以返回其他对象
    constructor(){
        return Object.create(null)
    }
}
console.log(new Fo() instanceof Fo) //true
console.log(new Foo() instanceof Foo) //flase

//与 ES5 一样，实例的属性除非显式定义在其本身（即定义在this对象上），否则都是定义在原型上（即定义在class上）
console.log(mp.hasOwnProperty('x')) //true
console.log(mp.hasOwnProperty('toString'))  //false

//类的所有实例共享一个原型对象
let mp2=new Mpoint(3,4)
console.log(mp.__proto__===mp2.__proto__); //true
//所以通过原型添加的方法，所有的实列都会拥有 不推荐使用


class MFoo{
    //是静态方法和属性
    // static myprop=34;
    static classMethod(){
        return 'hello'
    }
}
console.log(MFoo.myprop)
console.log(MFoo.classMethod())//hello
//静态方法在类上直接调用，但不能被实例调用
let fo=new MFoo()
// console.log(fo.classMethod()) 报错

class Bar extends MFoo{
    //可以继承父类静态方法
}
console.log(Bar.classMethod())//hello 











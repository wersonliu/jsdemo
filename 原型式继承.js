var person = {
    name: '六大圣',
    friends: ['小七', '大狗子', '二蛋']
};

var son = Object.create(person);
son.name = '小灵通';
son.friends.push('丫头');

console.log(son.friends); //[ '小七', '大狗子', '二蛋', '丫头' ]
console.log(son.name); //小灵通

// 引用值类型属性 person.friends  被共享
console.log(person.friends); //[ '小七', '大狗子', '二蛋', '丫头' ]
console.log(person.name); //六大圣

//这种继承可以改造为函数式调用 改造 学术上称 为寄生式继承
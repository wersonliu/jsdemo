const Event = require('events');

const event1 = new Event();
event1.on('come', function () {
    console.log('i shi shui')
});

event1.on('come', function () {
    console.log('i shi shui2')
});

event1.prependListener('come', function () {
    console.log('i shi shui3')
});

event1.emit('come');
//输出如下
// i shi shui3
// i shi shui
// i shi shui2

/***
 *
 * 其他类通过 util.inherits  或者es6的extends 可以继承这种能力
 * */

const EventEmitter = require('events');
class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter();
myEmitter.on('event', function(a, b) {
    console.log(a, b, this, this === myEmitter);
    // 打印:
    //   a b MyEmitter {
    //     domain: null,
    //     _events: { event: [Function] },
    //     _eventsCount: 1,
    //     _maxListeners: undefined } true
});
myEmitter.emit('event', 'a', 'b');

const myEmitter1 = new MyEmitter();
myEmitter1.on('event', (a, b) => {
    console.log(a, b, this);
    // 打印: a b {}
});
myEmitter1.emit('event', 'a', 'b');



// 使用 eventEmitter.once() 可以注册最多可调用一次的监听器。 当事件被触发时，监听器会被注销，然后再调用。

const myEmitter2 = new MyEmitter();
let m = 0;
myEmitter2.once('event', () => {
    console.log(++m);
});
myEmitter2.emit('event');
// 打印: 1
myEmitter2.emit('event');
// 不触发 ,已经销毁了


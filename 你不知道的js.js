function identify() {
    return this.name.toUpperCase();
}

function speak() {
    var greeting = "hello,i'm" + identify.call(this);
    console.log(greeting)
}

var me = {name: 'a李小龙'};
var you = {name: 'b李云龙'};

console.log(identify.call(me)); //A李小龙
console.log(identify.call(you));//B李云龙

speak.call(you);//hello,i'm李云龙

/**
 * this不简单的指向自身
 */

function f() {
    f.count = 4;//
    console.log(f.count)
}

f(); //4
console.log(f.count);  //4  f指向函数自身

// setTimeout(function () {
//     console.log('1秒后')  //匿名函数 无法指向自身
//
//     // 匿名函数指向自身的方法 arguments.callee
// }, 0);

function foo(num) {
    console.log('foo:' + num);
    this.count++;
}

foo.count = 0;

for (let i = 0; i < 10; i++) {
    if (i > 5) {
        //call 确保this指向函数对象foo本身
        foo.call(foo, i)
    }
}
console.log(foo.count); //4
//this 无法引用词法作用域内的东西
//作用域“对象”无法通过JavaScript
// 代码访问，它存在于JavaScript 引擎内部

/**
 * 如何（从调用栈中）分析出真正的调用位置的，因为它决定了this 的绑定
 * */
function foo1() {
    console.log(this.c);
}

var c = 3;
foo1(); //ide 打印时undefined  命令行是3
//foo1 在全局作用域中被调用,应用this的默认绑定,this指向全局
//foo1() 是直接使用不带任何修饰的函数引用进行调用的，因此只能使用
// 默认绑定

function foo2() {
    "use strict";
    console.log(this.a);
}

let a = 2;

// foo2(); // 严格模式调用报错


function foo3() {
    console.log(this.a3);
}

var a3 = 2;
(function () {
    "use strict";
    foo3(); // 2
})();

//隐式绑定
function foo4() {
    // console.log(this.ag);//22
    console.log(obj.ag);//22
    /**当foo4在obj内调用时,this指向了obj
     */
}

var obj = {
    ag: 22,
    foo4: foo4
};
obj.foo4();

// setTimeout(obj.foo4,1000);
// function foo() {
//     console.log( this.d );
// }
// var obj2 = {
//     d: 42,
//     foo: foo
// };
// var obj1 = {
//     d: 2,
//     obj2: obj2
// };
// obj1.obj2.foo(); // 42


function foo5(p1, p2) {
    this.val = p1 + p2;
}

// 之所以使用null 是因为在本例中我们并不关心硬绑定的this 是什么
// 反正使用new 时this 会被修改
console.log('##################');
var bar = foo5.bind(12, "p3");
console.log(bar.val);
var baz = new bar("p2");
console.log(baz.val); // p1p2

/***
 判断this
 现在我们可以根据优先级来判断函数在某个调用位置应用的是哪条规则。可以按照下面的
 顺序来进行判断：
 1. 函数是否在new 中调用（new 绑定）？如果是的话this 绑定的是新创建的对象。
 var bar = new foo()
 2. 函数是否通过call、apply（显式绑定）或者硬绑定调用？如果是的话，this 绑定的是
 指定的对象。
 var bar = foo.call(obj2)
 3. 函数是否在某个上下文对象中调用（隐式绑定）？如果是的话，this 绑定的是那个上
 下文对象。
 var bar = obj1.foo()
 4. 如果都不是的话，使用默认绑定。如果在严格模式下，就绑定到undefined，否则绑定到
 全局对象。
 var bar = foo()
 就是这样。对于正常的函数调用来说，理解了这些知识你就可以明白this 的绑定原理了。
 不过……凡事总有例外。

 如果你把null 或者undefined 作为this 的绑定对象传入call、apply 或者bind，这些值
 在调用时会被忽略，实际应用的是默认绑定规则

 使用bind()方法使函数拥有预设的初始参数，这些参数会排在最前面，传给绑定函数的参数会跟在它们后面
 */

function foo6(a, b) {
    console.log('a:' + a + ",b:" + b)

}
foo6.apply(null, [2, 3]);
//两个null都是占位作用
var bar1=foo6.bind(null,2);//a:2,b:3
bar1(4); //a:2,b:4

console.log('#################');
function foo6(a,b) {
    console.log( "a:" + a + ", b:" + b );
}
// 空对象
var ø = Object.create( null );
// 把数组展开成参数
foo6.apply( ø, [6, 7] ); // a:6, b:7
// 使用bind(..) 进行柯里化
var bar3 = foo6.bind( ø, 5 );
bar3( 8 ); // a:5, b:8
const Sequelize = require('sequelize');

const config = {
    database: 'spider_data',
    username: 'root',
    password: 'root',
    host: '127.0.0.1',
    port: 3306
};

const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 30000
    }
});
//有数字自增主键的情况下，不需要在模型指定
const User = sequelize.define('user', {
    // id: {
    //     type:Sequelize.INT,
    //     primaryKey: true
    // },
    name: Sequelize.STRING(100),
    gender: Sequelize.STRING(50)
}, {
    timestamps: false
});
//新增
var add = async () => {
    let lius = await User.create({
        // id: 'd-' + now,
        name: '刘伟2',
        gender: '女'
    });
    console.log('创建了' + JSON.stringify(lius))
};
// add();
//
// 查
var finda = async () => {
    let a = await User.findAll();
    for (let i of a) {
        console.log(JSON.stringify(i))
    }
};
// finda()


//根据id更新
var queryFormSomeWhere = async (id) => {
    //查询会返回数组
    var s = await User.findAll({
        where: {
            id: id
        }
    });
    console.log(JSON.stringify(s));
    return s[0]
};

var up = async (mid) => {

    let a = await queryFormSomeWhere(mid);
    a.name = '刘德华';
    console.log(JSON.stringify(a));
    await a.save();

};
// up(1);

//根据id删除
var dele = async (mid) => {
    let a = await queryFormSomeWhere(mid);
    await a.destroy();
};
// dele(1);


const Lagou = sequelize.define('spider_lagou', {
    keyword: Sequelize.STRING(),
    title: Sequelize.STRING(),
    city: Sequelize.STRING(),
    salary: Sequelize.STRING(),
    experience: Sequelize.STRING(),
    company: Sequelize.STRING(),
    education: Sequelize.STRING(),
    release_date: Sequelize.STRING(),
    source: Sequelize.STRING()
}, {
    timestamps: false
});
//已有的拉钩数据表操作
const getkey = async () => {
    let kw = await Lagou.findAll({
        attributes: [[Sequelize.literal('DISTINCT `keyword`'), 'keyword']]
    });
    let kw2 = await Lagou.findAll({
        attributes: [[Sequelize.literal('DISTINCT `city`'), 'city']]
    });
    let kw3 = await Lagou.findAll({
        attributes: [[Sequelize.literal('DISTINCT `education`'), 'education']]
    });
    let y=[];
    for (let x of kw){
        y.push(JSON.parse(JSON.stringify(x)))
    }
    for (let x of kw2){
        y.push(JSON.parse(JSON.stringify(x)))
    }
    for (let x of kw3){
        y.push(JSON.parse(JSON.stringify(x)))
    }
    console.log(y);
};
getkey();
const getjob = async (params) => {
    let jobs = await Lagou.findAll({
        where: {
            ...params
        }
    });
    for (let i of jobs) {
        console.log(JSON.stringify(i));
    }
};
// const mobj = {
//
// };
// getjob(mobj);


function print() {
    const args = Array.prototype.slice.call(arguments);  //为了使用unshift数组方法，将argument转化为真正的数组
    // args.unshift('(app)');
    console.log.apply(console, args);
}

print('你好');


let book = {
    "name": '刘生',
    author: {
        firstname: '刘',
        lastname: '伟'
    }
};
//js对象 关联数组
console.log(book.author.firstname);
console.log(book['name']);
//创建一个空对象

var o2 = Object.create(null); //o2是一个没有任何基础方法和属性的对象
console.log(o2);

var o3 = Object.create(Object.prototype);
console.log(o3);
var o4 = new Object();
console.log(o4);
console.log(o2 === o3);

/**
 * 属性检测
 **/

//判断对象自有或者继承属性  true
console.log('toString' in o3);
console.log(o3.toString !== undefined);

// 检测自有属性 下面返回false
console.log(o3.hasOwnProperty('toString'));//false
//检测自有属性是否可以枚举  下面返回false
console.log(o3.propertyIsEnumerable('toString'));//false


var o = {x: 1, y: 2, z: 3};
console.log(o.toString());//[object Object]
console.log(o.toLocaleString());//[object Object]
for (p in o) {
    console.log(p)
    //这里不会输出 toString
}
console.log('toString' in o); //true
// let res = Object.prototype.toString.call(o).slice(8, -1);
// console.log(res);
s = JSON.stringify(o);
console.log(s);
//{"x":1,"y":2,"z":3}|
p = JSON.parse(s);
console.log(p);
//{ x: 1, y: 2, z: 3 }  现在p是o的深度拷贝


/**
 *数组 方法
 **/


a = [1, 2, 3];
Object.defineProperty(a, "length", {writable: false});
a.length = 0;
print(a.length);//受保护的属性不可被修改

let b = ['a', 1, 2, null, undefined, 4, 'liu'];
for (let a of b) {
    //逐一循环
    // console.log(a);

    //跳过null 和undefined循环
    if (!a) continue;
    console.log(a)
}
//slice 返回数组的子数组
console.log(b.slice(0, 1)[0]); //a

//splice 返回切出来的数组,并且修改原数组
console.log(b.splice(4));

// pop  shift 返回被拿出来的数组元素

let sum = '';
b.forEach(function (value) {
    sum += value
});
console.log(sum);//数组求和  a12null


let bb = b.map(function (x) {
    return x * x
});
console.log(bb); //[ NaN, 1, 4, 0 ]


let bbb = b.every(function (x) {
    return x < 10
});
// 值小于10的返回true  空数组也返回true
console.log(bbb);

let bbbb = b.some(function (x) {
    return x < 10
});
// 所有返回值小于10的返回true
console.log(bbbb);

let a1 = [1, 3, 4, 5, 6, 2];
let su = a1.reduce(function (x, y) {
    return x + y
}, 0);
console.log(su); //数组求和 19

let su1 = a1.reduce(function (x, y) {
    return (x > y) ? x : y
}, 0);
console.log(su1);//6  数组求最大值

console.log(a1.indexOf(3));
//搜索是否存在这个元素,存在返回1,不存在返回负一
//indexof可以指定位置开始搜索

//类数组对象  本身没有join方法,但可以原型绑定
let q = {'0': 'a', '1': 'b', '2': 'c', length: 3};
// console.log(q.join(''));  报错
console.log(Array.prototype.join.call(q, '+'));

let w = 'javascript';  //字符串的某些行为类似只读数组
console.log(w.charAt(0));//t
console.log(w[1]); //e
// console.log(w.join(' '));  报错
let res = Array.prototype.filter.call(w, function (x) {
    return x.match(/[^aeiou]/);
}).join('');
console.log(res);  //通过原型过滤字符串   jvscrpt


/**
 *  函数
 *
 * */

var scope = 'global';
function f() {
    var scope = 'local';
    //这个函数使用的不是局部作用域  很少用到这种写法
    // return new Function('return scope');

    return function () {
        return scope
    };
}

console.log(f()()); //local

const articles=[
    {title:'啊啊',rating:4},
    {title:'啊啊1',rating:3},
    {title:'啊啊2',rating:2},
    {title:'啊啊3',rating:1.5},
    {title:'啊啊4',rating:7}
]
// 修改数组
const amazingArticles=articles.map((article)=>{
    return Object.assign(article,{rating:4.5})
})

console.log(amazingArticles)
console.log(articles)
// 过滤

const newArr=articles.filter((article)=>{
    return article.rating>=3
})

console.log(newArr)
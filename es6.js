let mobj = {
    a: 1,
    b: 2,
    c: 3,
    [Symbol()]: 0
};
//不含symbol的键
for (let i in mobj) {
    console.log(i)
}

console.log(Object.keys(mobj));
// console.log(Object.values(mobj));

//不含symbol 包括不可枚举的
let x = Object.getOwnPropertyNames(mobj);
console.log(x);

//获取所有的symbol属性的键
let y = Object.getOwnPropertySymbols(mobj);
console.log(y);

//获取所有的键
let z = Reflect.ownKeys(mobj);
console.log(z);

/**
 a
 b
 c
 [ 'a', 'b', 'c' ]
 [ 'a', 'b', 'c' ]
 [ Symbol() ]
 [ 'a', 'b', 'c', Symbol() ]
 */

console.log('xxxxx');

let a = {
    a: 1,
    b: 2,
    c: 3
};

let aClone = {...a};
console.log(aClone);
/**
 * 对象的扩展运算符等同于assign
 */
let bClone = Object.assign({}, a);
console.log(bClone);
console.log('xxxxx');


function m1({x = 0, y = 0} = {}) {
    return [x, y]
}

function m2({x, y} = {x: 0, y: 0}) {
    return [x, y]
}

console.log(m1());
console.log(m2());

console.log(m1({x: 3}));
console.log(m2({x: 3}));

// 函数的length 等于函数的参数减去默认参数

console.log((function m1({x = 0, y = 0} = {}) {
    return [x, y]
}).length);


let counters = [
    {id: 1, value: 0},
    {id: 2, value: 4},
    {id: 3, value: 0},
    {id: 4, value: 6},
    {id: 5, value: 0}
]
let nes = counters.map(function (item, index) {
    item.value = 0
    return item
});
console.log(nes)

let params = {
    city: '上海',
    education: undefined,
    keyword: undefined
};
let mp = {};
for (let i in params) {
    if (params[i] !== undefined) {
        mp[i] = params[i]
    }
}

console.log(mp);

const sss = [{keyword: 'java'},
    {keyword: 'web前端'},
    {keyword: 'nodejs'},
    {keyword: 'python'},
    {keyword: 'go'},
    {keyword: '产品经理'},
    {keyword: 'ui设计'},
    {city: '深圳'},
    {city: '广州'},
    {city: '杭州'},
    {city: '北京'},
    {city: '上海'},
    {city: '南京'},
    {city: '长沙'},
    {city: '济南'},
    {city: '珠海'},
    {city: '合肥'},
    {city: '成都'},
    {city: '大连'},
    {city: '武汉'},
    {city: '青岛'},
    {city: '厦门'},
    {city: '天津'},
    {city: '福州'},
    {city: '南宁'},
    {city: '镇江'},
    {city: '西安'},
    {city: '长春'},
    {city: '重庆'},
    {city: '苏州'},
    {city: '沈阳'},
    {city: '无锡'},
    {city: '昆明'},
    {city: '惠州'},
    {city: '宁波'},
    {city: '中山'},
    {city: '太原'},
    {city: '东莞'},
    {city: '乌鲁木齐'},
    {city: '郑州'},
    {city: '佛山'},
    {city: '日照'},
    {city: '贵阳'},
    {city: '桂林'},
    {city: '常州'},
    {city: '汕头'},
    {city: '石家庄'},
    {city: '海外'},
    {education: '本科'},
    {education: '大专'},
    {education: '不限'},
    {education: '硕士'}]

const xxxx = [{keyword: 'web前端'},
    {keyword: 'nodejs'},
    {keyword: 'python'},
    {keyword: 'go'},
    {keyword: '产品经理'},
    {keyword: 'ui设计'}];

let mx = xxxx.map((item,index) => {
    return {value: index, label: item['keyword']}
});
console.log(mx);


let mstr='where'
let test={
    city:'北京',
    keyword:'java',
    education:'大专'
};
// if (test==='{}'){
//     console.log(1);
// } else {
//     console.log(2);
// }

for(let k in test){
    if (test[k]!==undefined){
        mstr+=` ${k}='${test[k]}' and`
    }
}
mstr=mstr.substring(0,mstr.length-4);
let sql=`select * from spider_lagou ${mstr}`;
console.log(sql);

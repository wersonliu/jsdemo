function Foo(name) {
    this.name = name
}

Foo.prototype.myName = function () {
    return this.name
};

function Bar(name, label) {
    Foo.call(this, name);
    this.label = label;
}

// Bar.prototype=Object.create(Foo.prototype);

// var a = new Bar('我是bar');
//此时 Bar.prototype 并没有绑定到 Foo的原型上
// console.log(a.myName()); 这个会报错
//加上这个
Bar.prototype = Object.create(Foo.prototype);

var a = new Bar('我是bar','我是bar的label的属性');
console.log(a.myName());// 正常输出 '我是bar'
console.log(a.label); //我是bar的label的属性


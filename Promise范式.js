function test(resolve, reject) {
    var timeOut = Math.random() * 2;
    console.log('set timeout to:' + timeOut + 'seconds');
    setTimeout(function () {
        if (timeOut < 1) {
            console.log('调用resolve');
            resolve('200 ok')
        }
        else {
            console.log('调用reject');
            reject('set timeout in:' + timeOut + 'seconds')
        }
    }, timeOut * 1000)
}

// function fsuccess(res) {
//     console.log(res)
// }
//
// function failed(res) {
//     console.log(res)
// }
//
// test(fsuccess, failed);


// var p1 = new Promise(test);
// var p2 = p1.then(function (result) {
//     console.log('成功:' + result)
// });
// var p3 = p1.catch(function (result) {
//     console.log('失败:' + result)
// });

new Promise(test).then(function (result) {
    console.log('成功:' + result)
}).catch(function (resaon) {
    console.log('失败:' + resaon)
});

const buf = Buffer.from('hello', 'ascii');

console.log(buf.toString('hex'));
console.log(buf.toString('base64'));
console.log(buf.toString('utf-8'));

console.log('你好%s', '数据');
console.error(new Error('错误信息'));
// 打印: [Error: 错误信息]到 stderr。
const name = '描述';
console.warn(`警告${name}`);

console.log('你好世界');



const os=require('os');
console.log(os.cpus());
console.log(os.freemem());
console.log(os.hostname());
console.log(os.homedir());


